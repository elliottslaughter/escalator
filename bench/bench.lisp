;;;; Escalator -- Entity System for Common Lisp
;;;;   High Performance Object System for Games
;;;;
;;;; Copyright (c) 2010, Elliott Slaughter <elliottslaughter@gmail.com>
;;;;
;;;; Permission is hereby granted, free of charge, to any person
;;;; obtaining a copy of this software and associated documentation
;;;; files (the "Software"), to deal in the Software without
;;;; restriction, including without limitation the rights to use, copy,
;;;; modify, merge, publish, distribute, sublicense, and/or sell copies
;;;; of the Software, and to permit persons to whom the Software is
;;;; furnished to do so, subject to the following conditions:
;;;;
;;;; The above copyright notice and this permission notice shall be
;;;; included in all copies or substantial portions of the Software.
;;;;
;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;; EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;; MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
;;;; NONINFRINGEMENT.  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
;;;; HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
;;;; WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
;;;; OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.
;;;;

(in-package :escalator-bench)

;;;
;;; Simple ESCL Test
;;;

(defmacro autodef-escl-component (name (&rest dependencies) (&rest fields))
  `(defcomponent ,name (,@dependencies) (,@fields)))

(defmacro autodef-escl-system (name (&rest dependencies) (&rest fields))
  (let ((entity (gensym))
        (component (gensym)))
    `(defsystem ,name (,entity ,component ,@dependencies)
       ,@(iter (for field in fields)
               (collect `(incf (,field ,component)))))))

(defmacro autodef-escl-test-setup (name (&rest field-args) n-entities)
  `(iter (repeat ,n-entities)
         (make-entity nil '(,name) ,@(iter (for arg in field-args)
                                           (collect arg)
                                           (collect 0)))))

(defmacro autodef-escl-test-loop (n-iterations)
  `(time (iter (repeat ,n-iterations)
               (system-loop))))

(defmacro autodef-escl-test (testname n-fields n-entities n-iterations)
  (assert (integerp n-fields))
  (let* ((name (gensym))
         (dependencies nil)
         (fields (iter (repeat n-fields) (collect (gensym))))
         (field-args (iter (for field in fields)
                           (collect (intern (symbol-name field) :keyword)))))
    `(progn
       (autodef-escl-component ,name ,dependencies ,fields)
       (autodef-escl-system ,name ,dependencies ,fields)
       (defun ,testname ()
         (format
          t "ESCL: ~a iterations over ~a entities with ~a fields each.~%~%"
          ,n-iterations ,n-entities ,n-fields)
         (autodef-escl-test-setup ,name ,field-args ,n-entities)
         (autodef-escl-test-loop ,n-iterations)))))

(autodef-escl-test simple-escl-test 10 10000 1000)

(simple-escl-test)

;;;
;;; Simple CLOS Test
;;;

(defmacro autodef-clos-class (class (&rest superclasses)
                              (&rest slots) (&rest slot-args))
  `(defclass ,class (,@superclasses)
     ,(iter (for slot in slots)
            (for arg in slot-args)
            (collect `(,slot :accessor ,slot :initarg ,arg :accessor ,slot)))))

(defmacro autodef-clos-method (class method slots)
  (let ((instance (gensym)))
    `(progn
       (defgeneric ,method (,instance))
       (defmethod ,method ((,instance ,class))
         ,@(iter (for slot in slots)
                 (collect `(incf (slot-value ,instance ',slot))))))))

(defmacro autodef-clos-test-setup (class (&rest slot-args) n-instances)
  `(iter (repeat ,n-instances)
         (collect (make-instance ',class ,@(iter (for arg in slot-args)
                                                 (collect arg)
                                                 (collect 0))))))

(defmacro autodef-clos-test-loop (method instances n-iterations)
  (let ((instance (gensym)))
    `(time (iter (repeat ,n-iterations)
                 (iter (for ,instance in ,instances)
                       (,method ,instance))))))

(defmacro autodef-clos-test (testname n-slots n-instances n-iterations)
  (assert (integerp n-slots))
  (let* ((class (gensym))
         (superclasses nil)
         (slots (iter (repeat n-slots) (collect (gensym))))
         (slot-args (iter (for slot in slots)
                          (collect (intern (symbol-name slot) :keyword))))
         (method (gensym))
         (instances (gensym)))
    `(progn
       (autodef-clos-class ,class ,superclasses ,slots ,slot-args)
       (autodef-clos-method ,class ,method ,slots)
       (defun ,testname ()
         (format
          t "CLOS: ~a iterations over ~a instances with ~a slots each.~%~%"
          ,n-iterations ,n-instances ,n-slots)
         (let ((,instances
                (autodef-clos-test-setup ,class ,slot-args ,n-instances)))
           (autodef-clos-test-loop ,method ,instances ,n-iterations))))))

(autodef-clos-test small-clos-test 10 10000 1000)

(small-clos-test)
